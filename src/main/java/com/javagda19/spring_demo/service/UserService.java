package com.javagda19.spring_demo.service;

import com.javagda19.spring_demo.model.AppUser;

import java.util.List;

public interface UserService {
    void registerUser(String username, String password, String passwordConfirm);

    List<AppUser> getAllUsers();
}
