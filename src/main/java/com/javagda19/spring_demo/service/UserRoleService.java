package com.javagda19.spring_demo.service;

import com.javagda19.spring_demo.model.UserRole;

import java.util.Set;

public interface UserRoleService {
    Set<UserRole> getDefaultUserRoles();
}
